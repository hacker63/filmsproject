from flask_restful import Resource, reqparse
from flask import jsonify
from run import api, rpc_client
import json

class FilmsListAPI(Resource):
    def get(self):
        """
        Получение списка фильмов
        API для получения информации обо всех фильмах
        ---
        tags:
          - FilmsListAPI
        responses:
          200:
            description: Успешное выполнение запроса
            type: array
            items:
              schema:
                  id: Film
                  properties:
                    id:
                      type: integer
                      description: Уникальный идентификатор фильма
                    name:
                      type: string
                      description: Название фильма
                    duration:
                      type: integer
                      description: Длительность фильма
                    rating_imdb:
                      type: number
                      format: float
                      maximum: 10
                      minimum: 0
                      description: Рейтинг фильма
                    year_release:
                      type: string
                      description: Год релиза фильма
            """
        result = rpc_client.call({'action': 'get_films'})
        return jsonify(result)
    def post(self):
        """
        Добавление информации о новом фильме
        API для добавления информации о новом фильме
        ---
        tags:
          - FilmsListAPI
        parameters:
          - name: Параметры для добавления нового фильма
            in: body
            schema:
              properties:
                name:
                  type: string
                year_release:
                  type: string
                duration:
                  type: integer
                rating_imdb:
                  type: number
                  format: float
                  maximum: 10
                  minimum: 0
        responses:
          201:
            description: Фильм успешно добавлен
            schema:
              id: Film
              properties:
                id:
                  type: integer
                  description: Уникальный идентификатор фильма
                name:
                  type: string
                  description: Название фильма
                duration:
                  type: integer
                  description: Длительность фильма
                rating_imdb:
                  type: number
                  description: Рейтинг фильма
                year_release:
                  type: string
                  description: Год релиза фильма
        """
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, help='Название фильма имеет неверный формат',location='json')
        parser.add_argument('rating_imdb', type=float, help='Рейтинг фильма имеет неверный формат',location='json')
        parser.add_argument('duration', type=int, help='Длительность фильма имеет неверный формат',location='json')
        parser.add_argument('year_release', type=str, help='Год выпуска фильма имеет неверный формат',location='json')
        args = parser.parse_args()
        if None in args.values() or set(args.keys()) != {'name','year_release','duration','rating_imdb'}:
            result = {'status':400,'error':'Не все параметры были переданы'}
        else:
            body_dict = args
            body_dict['action'] = 'add_film'
            result = rpc_client.call(body_dict)
        return result, result.get('status')

class FilmAPI(Resource):
    def delete(self,id):
        """
        Удаление фильма
        API для удаления фильма
        ---
        tags:
          - FilmAPI
        consumes:
          - application/json
        produces:
          - application/json
        parameters:
          - in: path
            name: id
            required: true
            type: integer
        responses:
          200:
            description: Фильм успешно удалён
            schema:
              type: object
              properties:
                status:
                  type: integer
                  example: 200
                message:
                  type: string
                  example: Фильм успешно удалён
          404:
            description: Фильм не найден
            schema:
              type: object
              properties:
                status:
                  type: integer
                  example: 404
                message:
                  type: string
                  example: Фильм не найден
        """
        result = rpc_client.call({'action':'delete_film',
                                       'id':id})
        return result, result.get('status')
    def put(self,id):
        """
        Редактирование информации о фильме
        API для редактирования фильма
        ---
        tags:
          - FilmAPI
        consumes:
          - application/json
        produces:
          - application/json
        parameters:
          - in: path
            name: id
            required: true
            type: integer
          - name: Параметры для редактирования фильма
            in: body
            description: Параметры изменения информации о фильме.
            schema:
              properties:
                name:
                  type: string
                year_release:
                  type: string
                duration:
                  type: integer
                rating_imdb:
                  type: number
                  format: float
                  maximum: 10
                  minimum: 0
        responses:
          200:
            description: Информация о фильме успешно сохранена
            schema:
              type: object
              properties:
                status:
                  type: integer
                  example: 200
                message:
                  type: string
                  example: Информация о фильме успешно сохранена
          404:
            description: Фильм не найден
            schema:
              type: object
              properties:
                status:
                  type: integer
                  example: 404
                message:
                  type: string
                  example: Фильм не найден
        """
        parser = reqparse.RequestParser()
        parser.add_argument('name',
                            required=False,
                            type=str,
                            help='Название фильма имеет неверный формат',
                            location='json')
        parser.add_argument('rating_imdb',
                            required=False,
                            type=float,
                            help='Рейтинг фильма имеет неверный формат',
                            location = 'json')
        parser.add_argument('duration',
                            required=False,
                            type=int,
                            help='Длительность фильма имеет неверный формат',
                            location='json')
        parser.add_argument('year_release',
                            required=False,
                            type=str,
                            help='Год выпуска фильма имеет неверный формат',
                            location='json')
        args = parser.parse_args()
        body_dict = args
        body_dict['action'] = 'update_film'
        body_dict['id'] = id
        result = rpc_client.call(body_dict)
        return result,result.get('status')

class SearchFilm(Resource):
    def post(self):
        """
        Поиск фильмов
        API для поиска фильмов
        ---
        tags:
          - SearchFilm
        parameters:
        - name: Параметры поиска
          in: body
          description: Параметры для поиск фильмов.
          schema:
            properties:
              year_release:
                type: string
        responses:
          200:
            description: Успешное выполнение запроса
            type: array
            items:
              schema:
                id: Film
                properties:
                  id:
                    type: integer
                    description: Уникальный идентификатор фильма
                  name:
                    type: string
                    description: Название фильма
                  duration:
                    type: integer
                    description: Длительность фильма
                  rating_imdb:
                    type: number
                    format: float
                    maximum: 10
                    minimum: 0
                    description: Рейтинг фильма
                  year_release:
                    type: string
                    description: Год релиза фильма
        """
        parser = reqparse.RequestParser()
        parser.add_argument('year_release',
                            type=str,
                            required=True,
                            help='Год выпуска фильма имеет неверный формат',
                            location='json')
        args = parser.parse_args()
        search_str = args.get('year_release')
        body_dict = {
            'action': 'search_films',
            'search_str': search_str
        }
        result = rpc_client.call(body_dict)
        return result, result.get('status')

api.add_resource(FilmsListAPI, '/films', endpoint = 'films')
api.add_resource(FilmAPI, '/films/<int:id>', endpoint='film')
api.add_resource(SearchFilm, '/films/search', endpoint='search')
#!/usr/bin/env python
import pika
import json
from app import rpc_functions as rpc_func

""" Подключение к RabbitMQ и определение очереди для прослушивания """
connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()
channel.queue_declare(queue='rpc_queue')

def on_request(ch, method, props, body):
    """ Функция обработки сообщения очереди """
    body_dict = json.loads(body)
    response = None
    print('Response with payload <%s>' % body.decode())
    action = body_dict.get('action')
    if action == 'get_films':
        response = rpc_func.get_films()
    elif action == 'add_film':
        response = rpc_func.add_film(name=body_dict.get('name'),
                                     rating_imdb=body_dict.get('rating_imdb'),
                                     duration=body_dict.get('duration'),
                                     year_release=body_dict.get('year_release'))
    elif action == 'delete_film':
        response = rpc_func.delete_film(id=body_dict.get('id'))
    elif action == 'update_film':
        response = rpc_func.update_film(body_dict)
    elif action == 'search_films':
        response = rpc_func.search_films()
    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = props.correlation_id),
                     body=json.dumps(response))
    ch.basic_ack(delivery_tag = method.delivery_tag)

""" Определяем сколько сообщений может принимать воркер """
channel.basic_qos(prefetch_count=1)
""" Определяем какую очередь слушать и выставляем функцию-callback для его обработки """
channel.basic_consume(on_request, queue='rpc_queue')

print(" [x] Awaiting RPC requests")
""" Начало прослушивания очереди """
channel.start_consuming()
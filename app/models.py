from run_server import db

""" Модель базы данных для хранения информации о фильмах """
class Film(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False)
    year_release = db.Column(db.String, nullable=False)
    rating_imdb = db.Column(db.Float, nullable=False)
    duration = db.Column(db.Integer, nullable=False)
    def __repr__(self):
        return '<Film %r>' % self.name

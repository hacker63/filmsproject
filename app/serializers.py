from run_server import ma

""" Описываем схему для сериализации объектов SQLAlchemy """
class FilmSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('id', 'name', 'year_release', 'duration', 'rating_imdb')

film_schema = FilmSchema()
films_schema = FilmSchema(many=True)
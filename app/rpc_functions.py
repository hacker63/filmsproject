from run_server import db
from app.serializers import film_schema, films_schema
from app.models import Film
import json

def get_films():
    """ Функция получения списка фильмов """
    response = {
        'status': 200,
        'films': json.loads(films_schema.dumps(Film.query.all()).data)
    }
    return response

def add_film(name,rating_imdb,duration,year_release):
    """ Функция добавления нового фильма """
    if Film.query.filter_by(name=name).first() is not None:
        response = {'status': 400, 'error': 'Такой фильм уже существует'}
    elif rating_imdb < 0 or rating_imdb > 10:
        response = {'status': 400, 'error': 'Рейтиг фильма может быть от 0 до 10'}
    else:
        film = Film(name=name,
                    year_release=year_release,
                    rating_imdb=rating_imdb,
                    duration=duration)
        db.session.add(film)
        db.session.commit()
        response = json.loads(film_schema.dumps(film).data)
    return response

def delete_film(id):
    """ Функция удаления фильма """
    film = Film.query.get(id)
    if film is None:
        response = {'status': 404, 'message': 'Фильм не найден'}
    else:
        db.session.delete(film)
        db.session.commit()
        response = {'status': 200, 'message': 'Фильм успешно удалён'}
    return response

def update_film(body_dict):
    """ Функция обновления данных о фильме """
    response = {
        'status': 200,
        'message': 'Информация о фильме успешно сохранена'
    }
    editable_columns = {'name', 'year_release', 'duration', 'rating_imdb'}
    film = Film.query.get(body_dict.get('id'))
    searched_film = Film.query.filter_by(name=body_dict.get('name')).first() if 'name' in body_dict else None
    if film is None:
        response = {
            'status': 404,
            'message': 'Фильм не найден'
        }
    elif set(body_dict.keys()).intersection(editable_columns) == set():
        response = {
            'status': 400,
            'message': 'Параметры для изменения фильма отсутствуют'
        }
    elif searched_film is not None and searched_film.id != id:
        response = {
            'status': 400,
            'message': 'Имя фильма которое вы указали уже используется'
        }
    elif body_dict.get('rating_imdb') is not None and (
            body_dict.get('rating_imdb') < 0 or body_dict.get('rating_imdb') > 10):
        response = {'status': 400, 'error': 'Рейтиг фильма может быть от 0 до 10'}
    else:
        columns_insperation = set(body_dict.keys()).intersection(editable_columns)
        for column in columns_insperation:
            if body_dict.get(column) is not None:
                setattr(film, column, body_dict.get(column))
        db.session.commit()
    return response

def search_films(search_str):
    """ Функция поиска фильмов """
    if search_str is None:
        response = {
            'status': 400,
            'message': 'Параметры для поиска отсутствуют'
        }
    else:
        films = Film.query.filter_by(year_release=search_str).all()
        response = {
            'status': 200,
            'films': json.loads(films_schema.dumps(films).data)
        }
    return response
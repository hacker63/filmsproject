import pika
import uuid
import json

class FilmsRpcClient(object):
    def __init__(self):
        """ Инициализация объекта, создание подключения к RabbitMQ, определение нашей очереди """
        self.connect()
        result = self.channel.queue_declare(exclusive=True)
        self.callback_queue = result.method.queue
        self.channel.basic_consume(self.on_response,
                                no_ack=True,
                                queue=self.callback_queue)
    def connect(self):
        """ Метод подключения к RabbitMQ """
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost',heartbeat=60000))
        self.channel = self.connection.channel()
        self.channel.basic_qos(prefetch_count=1)
    def on_response(self, ch, method, props, body):
        """ Метод проверки соответствия id сообщения очереди с нашим uuid """
        if self.corr_id == props.correlation_id:
            self.response = json.loads(body)

    def call(self, body_dict):
        """ Метод для отправки сообщения в очередь """
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(exchange='',
                                   routing_key='rpc_queue',
                                   properties=pika.BasicProperties(
                                       reply_to=self.callback_queue,
                                       correlation_id=self.corr_id
                                   ),
                                   body=json.dumps(body_dict))
        while self.response is None:
            self.connection.process_data_events()
        return self.response

""" Хост и порт Flask приложения с API """
API_HOST = '127.0.0.1'
API_PORT = 9999

""" Режим разработки и тестирования """
DEBUG = False
TESTING = False

""" Формирование URI адреса к БД """
SQLALCHEMY_DATABASE_URI = ''

""" Если True, то Flask-SQLAlchemy будет отслеживать изменения объектов и давать сигналы """
SQLALCHEMY_TRACK_MODIFICATIONS = False

""" Подключаем локальный конфиг """
try:
    from local_settings import *
except:
    print('Ошибка при подключении локального конфига')
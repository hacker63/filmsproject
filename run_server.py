from flask import Flask
from flask_restful import Api
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
import pika

""" Инициализация Flask-приложения """
app = Flask(__name__)
""" Подключения конфигурационного файла """
app.config.from_pyfile('settings.py')

""" Подключение дополнительных пакетов """
api = Api(app)
ma = Marshmallow(app)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

""" Подключение к RabbitMQ и слушаем очередь """
connection = pika.BlockingConnection(pika.ConnectionParameters(host=app.config.get('RMQ_HOST','localhost'),
                                                               port=app.config.get('RMQ_PORT',5672)))
channel = connection.channel()
channel.queue_declare(queue='rpc_queue')

from app.rpc_server import *
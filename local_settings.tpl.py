""" Данные для доступа к БД """
DB_NAME = 'filmsdb'
DB_USER = 'filmsusr'
DB_PASSWORD = '123qweASD'
DB_HOST = 'localhost'
DB_PORT = 5432

""" Формирование URI адреса к БД """
SQLALCHEMY_DATABASE_URI = 'postgresql://{USER}:{PASS}@{HOST}:{PORT}/{DB}'.format(USER=DB_USER,
                                                                                 PASS=DB_PASSWORD,
                                                                                 HOST=DB_HOST,
                                                                                 PORT=DB_PORT,
                                                                                 DB=DB_NAME)

""" Параметры доступа к RabbitMQ """
RMQ_HOST = 'localhost'
RMQ_PORT = 5672

""" Режим разработки и тестирования """
DEBUG = True
TESTING = True
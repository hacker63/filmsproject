from flask import Flask
from flasgger import Swagger
from flask_restful import Api
from app.rpc_client import FilmsRpcClient

""" Инициализация Flask-приложения """
app = Flask(__name__)
""" Подключения конфигурационного файла """
app.config.from_pyfile('settings.py')

""" Подключение дополнительных пакетов """
swagger = Swagger(app)
api = Api(app)

""" Создание объекта для работы с RabbitMQ RPC """
rpc_client = FilmsRpcClient()

from app.controllers import *

if __name__ == '__main__':
    app.run(host=app.config.get('API_HOST','localhost'),
            port=app.config.get('API_PORT',5000))